var loaderVisible = true;
var countDownDate = new Date("May 6, 2022 15:30:00").getTime();

var myfunc = setInterval(() => {
    var now = new Date().getTime();
    var timeleft = countDownDate - now;

    if (timeleft < 0) {
        clearInterval(myfunc);
        document.getElementById("countdown").style.display = "none";
        document.getElementById("final").style.display = "block";

    } else {
        var days = Math.floor(timeleft / (1000 * 60 * 60 * 24));
        var hours = Math.floor((timeleft % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((timeleft % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((timeleft % (1000 * 60)) / 1000);

        document.getElementById("days").innerHTML = days
        document.getElementById("hours").innerHTML = hours > 9 ? hours : '0' + hours
        document.getElementById("mins").innerHTML = minutes > 9 ? minutes : '0' + minutes
        document.getElementById("secs").innerHTML = seconds > 9 ? seconds : '0' + seconds
    }

    if (loaderVisible) {
        loaderVisible = false;
        document.getElementById("loader").style.display = "none"
        if (timeleft > 0) {
            document.getElementById("countdown").style.display = "flex";
        }
    }

    let link = document.querySelector("#packinglist-link");
    let date = new Date(link.dataset.date);
    date.setHours(0);
    let dateNow = new Date();
    dateNow.setHours(0);
    if (date < dateNow) {
        link.classList.remove("hide");
    }

}, 1000);
