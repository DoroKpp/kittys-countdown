window.addEventListener('DOMContentLoaded', () => {
    document.querySelectorAll(".verse").forEach(verse => {
        let date = new Date(verse.dataset.date);
        date.setHours(0);
        let dateNow = new Date();
        dateNow.setHours(0);
        if (date < dateNow) {
            verse.classList.remove("hide");
        }
    });
});
